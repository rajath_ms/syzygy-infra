package com.lifing.syzygy.infra.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IRational;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.Utils;

public class VideoThumbnail {

	private static final String outputFilePrefix = "‎/Users/havannavar/Documents/workspace/";

	private static String processFrame(String thumbFilePath, BufferedImage image) {
		try {

			System.out.println(image);
			System.out.println(thumbFilePath);
			String outputFilename = thumbFilePath + System.currentTimeMillis() + ".jpeg";

			System.out.println(outputFilename);

			ImageIO.write(image, "jpeg", new File(outputFilename));

			System.out.println("at elapsed time of %6.3f seconds wrote: %s\n" + outputFilename);
			return outputFilename;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("deprecation")
	public static String generate(String filepath, String thumbFilePath, SuccessResponse response)
			throws NumberFormatException, IOException {

		// String filename = "Lemonop_Ad.mp4";

		if (!IVideoResampler.isSupported(IVideoResampler.Feature.FEATURE_COLORSPACECONVERSION))
			throw new RuntimeException(
					"you must install the GPL version of Xuggler (with IVideoResampler support) for this demo to work");

		IContainer container = IContainer.make();

		if (container.open(filepath, IContainer.Type.READ, null) < 0)
			throw new IllegalArgumentException("could not open file: " + filepath);

		String seconds = container.getDuration() / (1000000 * 2) + ""; // time of thumbnail

		int numStreams = container.getNumStreams();

		// and iterate through the streams to find the first video stream
		int videoStreamId = -1;
		IStreamCoder videoCoder = null;
		for (int i = 0; i < numStreams; i++) {
			// find the stream object
			IStream stream = container.getStream(i);
			// get the pre-configured decoder that can decode this stream;
			IStreamCoder coder = stream.getStreamCoder();

			if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
				videoStreamId = i;
				videoCoder = coder;
				break;
			}
		}

		if (videoStreamId == -1)
			throw new RuntimeException("could not find video stream in container: " + filepath);

		if (videoCoder.open() < 0)
			throw new RuntimeException("could not open video decoder for container: " + filepath);

		IVideoResampler resampler = null;
		if (videoCoder.getPixelType() != IPixelFormat.Type.BGR24) {

			resampler = IVideoResampler.make(videoCoder.getWidth(), videoCoder.getHeight(), IPixelFormat.Type.BGR24,
					videoCoder.getWidth(), videoCoder.getHeight(), videoCoder.getPixelType());
			if (resampler == null)
				throw new RuntimeException("could not create color space resampler for: " + filepath);
		}

		IPacket packet = IPacket.make();

		IRational timeBase = container.getStream(videoStreamId).getTimeBase();

		System.out.println("Timebase " + timeBase.toString());

		long timeStampOffset = (timeBase.getDenominator() / timeBase.getNumerator()) * Integer.parseInt(seconds);
		System.out.println("TimeStampOffset " + timeStampOffset);

		long target = container.getStartTime() + timeStampOffset;

		container.seekKeyFrame(videoStreamId, target, 0);

		boolean isFinished = false;

		String thumbFile = new String("");
		while (container.readNextPacket(packet) >= 0 && !isFinished) {

			if (packet.getStreamIndex() == videoStreamId) {

				IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(), videoCoder.getWidth(),
						videoCoder.getHeight());

				int offset = 0;
				while (offset < packet.getSize()) {

					int bytesDecoded = videoCoder.decodeVideo(picture, packet, offset);
					if (bytesDecoded < 0) {
						System.err.println("WARNING!!! got no data decoding " + "video in one packet");
					}
					offset += bytesDecoded;

					if (picture.isComplete()) {

						IVideoPicture newPic = picture;

						if (resampler != null) {

							newPic = IVideoPicture.make(resampler.getOutputPixelFormat(), picture.getWidth(),
									picture.getHeight());
							if (resampler.resample(newPic, picture) < 0)
								throw new RuntimeException("could not resample video from: " + filepath);
						}

						if (newPic.getPixelType() != IPixelFormat.Type.BGR24)
							throw new RuntimeException("could not decode video as BGR 24 bit data in: " + filepath);

						BufferedImage javaImage = Utils.videoPictureToImage(newPic);

						thumbFile = processFrame(thumbFilePath, javaImage);
						isFinished = true;
					}
				}
			}
		}

		if (videoCoder != null) {
			videoCoder.close();
			videoCoder = null;
		}
		if (container != null) {
			container.close();
			container = null;
		}
		return thumbFile;
	}

}