package com.lifing.syzygy.infra.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Performs the same exception handling as {@link ExceptionHandlingController}
 * but offers them globally. The exceptions below could be raised by any
 * controller and they would be handled here, if not handled in the controller
 * already.
 * 
 * @author havannavar
 */
@ControllerAdvice
public class PlatformExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ SyzBaseException.class })
	protected ResponseEntity<Object> handleInvalidRequest(RuntimeException e, WebRequest request) {
		SyzBaseException ire = (SyzBaseException) e;
		// List<FieldErrorResource> fieldErrorResources = new ArrayList<>();

		// List<FieldError> fieldErrors = ire.getErrors().getFieldErrors();
		// for (FieldError fieldError : fieldErrors) {
		// FieldErrorResource fieldErrorResource = new FieldErrorResource();
		// fieldErrorResource.setResource(fieldError.getObjectName());
		// fieldErrorResource.setField(fieldError.getField());
		// fieldErrorResource.setCode(fieldError.getCode());
		// fieldErrorResource.setMessage(fieldError.getDefaultMessage());
		// fieldErrorResources.add(fieldErrorResource);
		// }

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ErrorResource error = new ErrorResource(422, ire.getMessage());
		// error.setFieldErrors(fieldErrorResources);

		if (ire.getId().equals("CORE-INRFA-0002")) {
			error = new ErrorResource(401, ire.getMessage());
			return handleExceptionInternal(e, error, headers, HttpStatus.UNAUTHORIZED, request);
		} else if (ire.getId().equals("CORE-INRFA-0003")) {
			error = new ErrorResource(500, ire.getMessage());
			return handleExceptionInternal(e, error, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
		}
		return handleExceptionInternal(e, error, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);

	}
}
