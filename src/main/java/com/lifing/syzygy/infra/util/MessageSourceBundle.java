package com.lifing.syzygy.infra.util;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

//@Component("msgSourceBundle")
public class MessageSourceBundle {

	@Bean
	public static MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		String[] basenames = { "classpath:/aspirant-messages_en_US", "classpath:/infra-messages_en_US",
				"classpath:/employer-messages_en_US" };
		messageSource.setBasenames(basenames);
		messageSource.setDefaultEncoding("UTF-8");
		messageSource.setCacheSeconds(0);
		messageSource.setFallbackToSystemLocale(false);
		return messageSource;
	}
}
