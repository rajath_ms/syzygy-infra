package com.lifing.syzygy.infra.response;

public class Dashboard {

	private Object profile;
	private Object gigs;
	private Object hallOfFame;

	public Object getProfile() {
		return profile;
	}

	public void setProfile(Object profile) {
		this.profile = profile;
	}

	public Object getGigs() {
		return gigs;
	}

	public void setGigs(Object gigs) {
		this.gigs = gigs;
	}

	public Object getHallOfFame() {
		return hallOfFame;
	}

	public void setHallOfFame(Object hallOfFame) {
		this.hallOfFame = hallOfFame;
	}

}
