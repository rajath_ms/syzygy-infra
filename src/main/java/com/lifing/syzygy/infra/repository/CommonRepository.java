package com.lifing.syzygy.infra.repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.Document;
import org.springframework.data.mongodb.core.query.Query;

public interface CommonRepository {

	<T> T save(String jsonData, String collectionName);

	<T> T save(Document jsonData, String collectionName);

	<T> T get(String _id, String collectionName);

	<T> T get(String field, Object value, String collectionName);

	<T> List<T> getAll(Map<String, Object> requestParams, String collectionName);

	<T> T update(String jsonData, String collectionName, String _id);

	<T> T update(Document doc, String collectionName, String _id);

	<T> T update(Map<String, Object> queryParams, Map<String, Object> updateParams, String collectionName);

	<T> List<T> fullTextSearch(Map<String, Object> queryParams, List<String> includeFields, String collectionName);

	<T> List<T> fullTextSearch(Map<String, Object> queryParams, List<String> includeFields,
			Map<String, Object> operations, String collectionName);

	<T> List<T> textContains(Map<String, Object> requestParams, List<String> includeFields,
			Map<String, Object> operation, String collectionName);

	<T> List<T> sortRecords(Set<String> profileIds, List<String> skills, List<String> includeFields,
			Map<String, Object> params, String collectionName);

	void remove(String _id, String collectionName);

	void remove(String field, String value, String collectionName);

	/** fect all the details of a callection either include or exclude fields */
	<T> List<T> getAll(Map<String, Object> requestParams, List<String> fields, boolean include, String collectionName);

	<T> List<T> getElementMatch(Map<String, Object> requestParams, String collectionName);

	<T> List<T> getRecordsFromInQuery(String field, List<String> values, List<String> fields, boolean include,
			Map<String, Object> params, String collectionName);

	<T> List<T> firstJobRecommendations(List<Integer> values, Integer gigCount, List<String> fields,
			Map<String, Object> params, String collectionName);

	long countQuery(Query query, String string);

}
