package com.lifing.syzygy.infra.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.bson.Document;
import org.junit.BeforeClass;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public abstract class BaseTestCase {

	public static Document auth = null;
	
	@BeforeClass
	public static void setUp() {
		
		Resource resource = new ClassPathResource("auth.json");
		try {
			auth = Document.parse(new String(Files.readAllBytes(Paths.get(resource.getURI()))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
